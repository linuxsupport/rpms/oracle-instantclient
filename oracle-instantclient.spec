# pkgname is the upstream name
# name is the same as the upstream name (CC7) or prefixed with "cern-" (C8)
%define pkgname oracle-instantclient
#C8
%if 0%{?rhel} == 8
Name: cern-%{pkgname}
%else%
#CC7
Name: %{pkgname}
%endif%
Summary: META package for the ORACLE instantclient
Version: 19.3
Release: 4%{?dist}
Epoch:   2
Source1: oracle-instantclient11.2-x86_64.conf
Source2: oracle-instantclient12.2-x86_64.conf
Source3: pcscfg.cfg-%{version}.fix
License: GPL
Group: Applications/File
BuildRoot: %{_tmppath}/%{name}-root
ExclusiveArch: x86_64

%define versionlib 19.1

%description
This META package provides required dependency chain

###################################   version 11.2   ###############################

%package -n %{name}11.2-meta
Summary: META package for the ORACLE instantclient 11.2
Provides: libclntsh.so.11.2()(64bit)
Provides: libocci.so.11.2()(64bit)
Provides: libnnz11.so()(64bit)
Requires:  oracle-instantclient-tnsnames.ora

%description -n %{name}11.2-meta
This META package adds rpm provides missing in original Oracle
InstantClient packages and configures system LD_LIBRARY_PATH

%package -n %{name}11.2
Summary: META package for the ORACLE instantclient 11.2
Requires: %{name}11.2-meta

%description -n %{name}11.2
This META package adds rpm provides missing in original Oracle
InstantClient packages and configures system LD_LIBRARY_PATH

%package -n %{name}11.2-compat
Summary: compat package for the ORACLE instantclient 11.2
Requires: %{pkgname}11.2-basic
Conflicts: %{pkgname}11.2-devel

%description -n %{name}11.2-compat
This compat package adds unversioned libraries symbolic
links (libclntshcore.so, libocci.so, libclntsh.so) without
installing %{pkgname}11.2-devel


%triggerin -n %{name}11.2-meta -- %{pkgname}11.2-basic, %{pkgname}11.2-basiclite, %{pkgname}11.2-jdbc, %{pkgname}11.2-odbc, %{pkgname}11.2-sqlplus, %{pkgname}11.2-devel, %{pkgname}11.2-tools, %{pkgname}11.2-precomp %{pkgname}11.2-compat
/usr/sbin/ldconfig ||:

%triggerun -n %{name}11.2-meta -- %{pkgname}11.2-basic, %{pkgname}11.2-basiclite, %{pkgname}11.2-jdbc, %{pkgname}11.2-odbc, %{pkgname}11.2-sqlplus, %{pkgname}11.2-devel, %{pkgname}11.2-tools, %{pkgname}11.2-precomp %{pkgname}11.2-compat
/usr/sbin/ldconfig ||:

###################################   version 12.2   ###############################

%package -n %{name}12.2-meta
Summary: META package for the ORACLE instantclient 12.2
Provides: libclntsh.so.12.1()(64bit)
Provides: libocci.so.12.1()(64bit)
Provides: libnnz12.so()(64bit)
Requires: oracle-instantclient-tnsnames.ora

%description -n %{name}12.2-meta
This META package adds rpm provides missing in original Oracle
InstantClient packages and configures system LD_LIBRARY_PATH

%package -n %{name}12.2
Summary: META package for the ORACLE instantclient 12.2
Requires: %{name}12.2-meta

%description -n %{name}12.2
This META package adds rpm provides missing in original Oracle
InstantClient packages and configures system LD_LIBRARY_PATH

%package -n %{name}12.2-compat
Summary: compat package for the ORACLE instantclient 12.2
Requires: %{pkgname}12.2-basic
Conflicts: %{pkgname}12.2-devel

%description -n %{name}12.2-compat
This compat package adds unversioned libraries symbolic
links (libclntshcore.so, libocci.so, libclntsh.so) without
installing %{name}12.2-devel


%triggerin -n %{name}12.2-meta -- %{name}12.2-basic, %{name}12.2-basiclite, %{name}12.2-jdbc, %{name}12.2-odbc, %{name}12.2-sqlplus, %{name}12.2-devel, %{name}12.2-tools, %{name}12.2-precomp %{name}12.2-compat
/usr/sbin/ldconfig ||:

%triggerun -n %{name}12.2-meta -- %{name}12.2-basic, %{name}12.2-basiclite, %{name}12.2-jdbc, %{name}12.2-odbc, %{name}12.2-sqlplus, %{name}12.2-devel, %{name}12.2-tools, %{name}12.2-precomp %{name}12.2-compat
/usr/sbin/ldconfig ||:


################################### version %{version} ##################################

%package -n %{name}%{version}-meta
Summary: META package for the ORACLE instantclient %{version}
Provides: libclntsh.so.%{versionlib}()(64bit)
Provides: libocci.so.%{versionlib}()(64bit)
Provides: libnnz19.so()(64bit)
Requires: oracle-instantclient-tnsnames.ora

%package -n %{name}%{version}
Summary: META package for the ORACLE instantclient %{version}
Requires: %{name}%{version}-meta

%description -n %{name}%{version}
This META package adds rpm provides missing in original Oracle
InstantClient packages and configures system LD_LIBRARY_PATH

%description -n %{name}%{version}-meta
This META package adds rpm provides missing in original Oracle
InstantClient packages and configures system LD_LIBRARY_PATH

%package -n %{name}%{version}-compat
Summary: compat package for the ORACLE instantclient %{version}
Requires: %{pkgname}%{version}-basic
Conflicts: %{pkgname}%{version}-devel

%description -n %{name}%{version}-compat
This compat package adds unversioned libraries symbolic
links (libclntshcore.so, libocci.so, libclntsh.so) without
installing %{pkgname}%{version}-devel

%package -n %{name}-compat
Summary: compat package for the ORACLE instantclient
Requires: %{pkgname}-basic
Conflicts: %{pkgname}-devel

%description -n %{name}-compat
This compat package adds unversioned libraries symbolic
links (libclntshcore.so, libocci.so, libclntsh.so) without
installing %{pkgname}-devel

%package -n %{name}-basic
Summary: META package for the ORACLE instantclient (latest)
Requires: %{name}%{version}-meta
Requires: %{pkgname}%{version}-basic >= %{version}

%description -n %{name}-basic
Summary: META package for the ORACLE instantclient BASIC (latest)

%package -n %{name}-devel
Summary: META package for the ORACLE instantclient (latest)
Requires: %{name}%{version}-meta
Requires: %{pkgname}%{version}-devel >= %{version}

%description -n %{name}-devel
Summary: META package for the ORACLE instantclient BASIC (latest)

%package -n %{name}-basiclite
Summary: META package for the ORACLE instantclient (latest)
Requires: %{name}%{version}-meta
Requires: %{pkgname}%{version}-basiclite >= %{version}

%description -n %{name}-basiclite
Summary: META package for the ORACLE instantclient BASIC (latest)

%package -n %{name}-jdbc
Summary: META package for the ORACLE instantclient (latest)
Requires: %{name}%{version}-meta
Requires: %{pkgname}%{version}-jdbc >= %{version}

%description -n %{name}-jdbc
Summary: META package for the ORACLE instantclient BASIC (latest)

%package -n %{name}-odbc
Summary: META package for the ORACLE instantclient (latest)
Requires: %{name}%{version}-meta
Requires: %{pkgname}%{version}-odbc >= %{version}

%description -n %{name}-odbc
Summary: META package for the ORACLE instantclient BASIC (latest)

%package -n %{name}-precomp
Summary: META package for the ORACLE instantclient (latest)
Requires: %{name}%{version}-meta
Requires: %{pkgname}%{version}-precomp >= %{version}

%description -n %{name}-precomp
Summary: META package for the ORACLE instantclient BASIC (latest)

%package -n %{name}-tools
Summary: META package for the ORACLE instantclient (latest)
Requires: %{name}%{version}-meta
Requires: %{pkgname}%{version}-tools >= %{version}

%description -n %{name}-tools
Summary: META package for the ORACLE instantclient BASIC (latest)

%package -n %{name}-sqlplus
Summary: META package for the ORACLE instantclient (latest)
Requires: %{name}%{version}-meta
Requires: %{pkgname}%{version}-sqlplus >= %{version}
# this dependency is provided by glibc on CC7
%if 0%{?rhel} == 8
Requires: libnsl
%endif%

%description -n %{name}-sqlplus
Summary: META package for the ORACLE instantclient BASIC (latest)

%triggerin -n %{name}%{version}-meta -- %{pkgname}%{version}-basic, %{pkgname}%{version}-basiclite, %{pkgname}%{version}-jdbc, %{pkgname}%{version}-odbc, %{pkgname}%{version}-sqlplus, %{pkgname}%{version}-devel, %{pkgname}%{version}-tools, %{pkgname}%{version}-precomp %{pkgname}%{version}-compat
/usr/sbin/ldconfig ||:

%triggerun -n %{name}%{version}-meta -- %{pkgname}%{version}-basic, %{pkgname}%{version}-basiclite, %{pkgname}%{version}-jdbc, %{pkgname}%{version}-odbc, %{pkgname}%{version}-sqlplus, %{pkgname}%{version}-devel, %{pkgname}%{version}-tools, %{pkgname}%{version}-precomp %{pkgname}%{version}-compat
/usr/sbin/ldconfig ||:

%install

mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d/
install -m 644 %SOURCE1 $RPM_BUILD_ROOT/etc/ld.so.conf.d/
install -m 644 %SOURCE2 $RPM_BUILD_ROOT/etc/ld.so.conf.d/

mkdir -p $RPM_BUILD_ROOT/usr/lib/oracle/%{version}/client64/lib/precomp/admin/
install -m 644 %SOURCE3 $RPM_BUILD_ROOT/usr/lib/oracle/%{version}/client64/lib/precomp/admin/

mkdir -p $RPM_BUILD_ROOT/usr/lib/oracle/%{version}/client64/lib/
pushd $RPM_BUILD_ROOT/usr/lib/oracle/%{version}/client64/lib/
ln -s libclntshcore.so.%{versionlib} libclntshcore.so
ln -s libocci.so.%{versionlib} libocci.so
ln -s libclntsh.so.%{versionlib} libclntsh.so
popd

mkdir -p $RPM_BUILD_ROOT/usr/lib/oracle/11.2/client64/lib/
pushd $RPM_BUILD_ROOT/usr/lib/oracle/11.2/client64/lib/
ln -s libocci.so.11.1 libocci.so
ln -s libclntsh.so.11.1 libclntsh.so
popd

mkdir -p $RPM_BUILD_ROOT/usr/lib/oracle/12.2/client64/lib/
pushd $RPM_BUILD_ROOT/usr/lib/oracle/12.2/client64/lib/
ln -s libocci.so.12.1 libocci.so
ln -s libclntsh.so.12.1 libclntsh.so
popd


%clean
rm -rf $RPM_BUILD_ROOT

%post -n %{name}%{version}-meta

/usr/sbin/ldconfig ||:

%post -n %{name}11.2-meta

/usr/sbin/ldconfig ||:

%postun -n %{name}%{version}-meta

/usr/sbin/ldconfig ||:

%postun -n %{name}11.2-meta

/usr/sbin/ldconfig ||:

%post -n %{name}11.2-compat

/usr/sbin/ldconfig ||:

%postun -n %{name}11.2-compat

/usr/sbin/ldconfig ||:

%postun -n %{name}12.2-compat

/usr/sbin/ldconfig ||:

%post -n %{name}-precomp
/bin/cp -f  /usr/lib/oracle/%{version}/client64/lib/precomp/admin/pcscfg.cfg-%{version}.fix /usr/lib/oracle/%{version}/client64/lib/precomp/admin/pcscfg.cfg
exit 0

%triggerin -- oracle-instanclient%{version}-precomp
/bin/cp -f  /usr/lib/oracle/%{version}/client64/lib/precomp/admin/pcscfg.cfg-%{version}.fix /usr/lib/oracle/%{version}/client64/lib/precomp/admin/pcscfg.cfg
exit 0


%files -n %{name}%{version}-meta
%defattr(-,root,root)
/usr/lib/oracle/%{version}/client64/lib/libclntsh.so
/usr/lib/oracle/%{version}/client64/lib/libocci.so
/usr/lib/oracle/%{version}/client64/lib/libclntshcore.so

%files -n %{name}11.2-meta
%defattr(-,root,root)
/etc/ld.so.conf.d/oracle-instantclient11.2-x86_64.conf

%files -n %{name}11.2-compat
%defattr(-,root,root)
/usr/lib/oracle/11.2/client64/lib/libclntsh.so
/usr/lib/oracle/11.2/client64/lib/libocci.so

%files -n %{name}12.2-meta
%defattr(-,root,root)
/etc/ld.so.conf.d/oracle-instantclient12.2-x86_64.conf

%files -n %{name}12.2-compat
%defattr(-,root,root)
/usr/lib/oracle/12.2/client64/lib/libclntsh.so
/usr/lib/oracle/12.2/client64/lib/libocci.so

%files -n %{name}%{version}-compat
%defattr(-,root,root)

%files -n %{name}-basic
%defattr(-,root,root)

%files -n %{name}-basiclite
%defattr(-,root,root)

%files -n %{name}-devel
%defattr(-,root,root)

%files -n %{name}-jdbc
%defattr(-,root,root)

%files -n %{name}-odbc
%defattr(-,root,root)

%files -n %{name}-precomp
%defattr(-,root,root)
/usr/lib/oracle/%{version}/client64/lib/precomp/admin/pcscfg.cfg-%{version}.fix

%files -n %{name}-sqlplus
%defattr(-,root,root)

%files -n %{name}-tools
%defattr(-,root,root)

%files -n %{name}
%defattr(-,root,root)

%files -n %{name}%{version}
%defattr(-,root,root)

%files -n %{name}11.2
%defattr(-,root,root)

%post -n %{name}%{version}-compat

/usr/sbin/ldconfig ||:

%postun -n %{name}%{version}-compat

/usr/sbin/ldconfig ||:



%changelog
* Wed Mar 04 2020 Ben Morrice <ben.morrice@cern.ch> - 19.3-4
- build for C8

* Wed Jan 29 2020 Ben Morrice <ben.morrice@cern.ch> - 19.3-3
- remove more obsoletes

* Wed Nov 06 2019 Ben Morrice <ben.morrice@cern.ch> - 19.3-2
- remove obsoletes
- add 12.2 compat and meta packages

* Fri Oct 04 2019 Ben Morrice <ben.morrice@cern.ch> - 19.3-1
- bump to version 19.3

* Wed Dec 20 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 12.2-7
- fix compat package symbolic links 

* Mon Oct 09 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 12.2-6
- fix metapackage provides.

* Fri Sep 15 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 12.2-4
- added obsolete for oracle-instantclient12.1-sqlplus
- updated for 12.2.0.1

* Tue Jul 25 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 12.1-12
- added -tools metapackage.

* Mon Nov 28 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 12.1-10
- oracle-instantclient{X.Y}-compat added

* Thu Nov 24 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 12.1-9
- triggerin/triggerun for meta packages to run ldconfig

* Thu Dec 03 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 12.1-7
- add post/triggerin for -precomp package settign correct pcscfg.cfg

* Thu Dec 03 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 12.1-6
- fix obsoletes/requires

* Tue Dec 01 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.2-1

- provide metapackages for old naming schema (oracle-instantclient-sthg)

* Thu Nov 26 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1-1

- change numbering schema, repackage to allow multiple versions 
  of instantclient installs.

* Wed Nov 25 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 11.2-1
- turns out we need 11.2 too on 7

* Mon Oct 27 2014 Matthias Schroder <Matthias.Schroder@cern.ch> - %{version}-3
- enable provide for libnnz12.so

* Fri Sep 12 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - %{version}-2
- fix reqs/provs.

* Wed Sep 10 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - %{version}.0.2
- el7 initial build

