# oracle-instantclient

## Introduction

* This repo/spec builds 'meta' packages which compliment the upstream packages provided/built by Oracle
* The packages are built via gitlab-ci and require the upstream Oracle packages to be present to be tested successfully
* Note: when upgrading to a new a major version, be sure to check if we need to build oracle-instantclient{$OLDVERSION}-{meta,compat}-${NEWVERSION} packages. Check the spec file for more details.

## CC7 vs C8 differences (History)

* Historically (CC7) the meta packages that this repository creates were named `oracle-instantclient-$product`. $product being `{basic,basiclite,compat,devel,sqlplus,etc}`.
* From the 19.3 release, Oracle now defines in their `oracle-instantclient19.3-basic-19.3.0.0.0-1.x86_64.rpm` package 'Conflicts' for `oracle-instantclient-basic`. This made it impossible to use the upstream Oracle RPMs using our meta packages. To work around the issue linuxsupport 'rebuilt' the rpm, removing these conflicts. Rebuilding in this context is not referring to `rpmbuild` - but instead the tool `rpmrebuild`. Whilst this works, it's a hack and should be avoided... Which brings us to C8
* In order to use the upstream Oracle packages without modifying them, on C8 the meta packages created from this repository have a different name to avoid future conflicts: `cern-oracle-instantclient`.

## Building a new rpm

* Simply edit the spec file and gitlab-ci/koji will do the rest

## Sourcing with upstream Oracle packages

* One can grab most of the required rpms via the linuxsoft oracle mirror:
```
for i in basic basiclite devel jdbc odbc sqlplus tools; do curl -O http://linuxsoft.cern.ch/mirror/yum.oracle.com/repo/OracleLinux/OL7/oracle/instantclient/x86_64/getPackage/oracle-instantclient19.3-$i-19.3.0.0.0-1.x86_64.rpm; done
```
* This will get all packages except 'oracle-instantclient19.3-precomp' which needs to be downloaded manually via [oracle.com](https://www.oracle.com/database/technologies/instant-client/precompiler-112010-downloads.html) with a free to subscribe Oracle account

## Legacy (CC7) update information

Note - this information is only relevant if adding a NEW upstream Oracle Instantclient version for CC7. It is most likely not going to be required as we already have 19.3 in CC7; though most users are still using 12.2. Upgrades will likely occur when users move to C8.

### Editing the Conflicts: in oracle-instantclient19.3-basic upstream rpm
* Unfortunately for us, Oracle has decided in version 19.x to Conflict many previous packages including the name 'oracle-instantclient-basic'. This completely breaks our meta package installs. To fix, we need to `rpmrebuild` the RPM from oracle and remove the Conflicts defined in the oracle-instantclient-basic RPM

Before modifications:
```
$ rpm -qp --conflicts oracle-instantclient19.3-basic-19.3.0.0.0-1.x86_64.rpm
warning: oracle-instantclient19.3-basic-19.3.0.0.0-1.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID ec551f03: NOKEY
oracle-xe
oracle-database-xe-18c
libclntsh.so < 19.1
oracle-instantclient-basic
oracle-instantclient-basiclite
oracle-instantclient11.1-basic
oracle-instantclient11.1-basiclite
oracle-instantclient11.2-basic
oracle-instantclient11.2-basiclite
oracle-instantclient12.1-basic
oracle-instantclient12.1-basiclite
libclntsh.so.12.1
libclntsh.so.18.1
libclntsh.so.19.1
oracle-database-ee-18c
oracle-database-ee-19c
oracle-database-ee-20c
```

Patching with rpmrebuild:

`rpmrebuild -enp oracle-instantclient19.3-basic-19.3.0.0.0-1.x86_64.rpm`

Before:
```
Conflicts:     oracle-xe
Conflicts:     oracle-database-xe-18c
Conflicts:     libclntsh.so < 19.1
Conflicts:     oracle-instantclient-basic
Conflicts:     oracle-instantclient-basiclite
Conflicts:     oracle-instantclient11.1-basic
Conflicts:     oracle-instantclient11.1-basiclite
Conflicts:     oracle-instantclient11.2-basic
Conflicts:     oracle-instantclient11.2-basiclite
Conflicts:     oracle-instantclient12.1-basic
Conflicts:     oracle-instantclient12.1-basiclite
Conflicts:     libclntsh.so.12.1
Conflicts:     libclntsh.so.18.1
Conflicts:     libclntsh.so.19.1
Conflicts:     oracle-database-ee-18c
Conflicts:     oracle-database-ee-19c
Conflicts:     oracle-database-ee-20c
Provides:      libclntsh.so.19.1
Provides:      libclntsh.so = 19.1
Provides:      oracle-instantclient19.3-basic = 19.3.0.0.0-1
Provides:      oracle-instantclient19.3-basic(x86-64) = 19.3.0.0.0-1
Requires:      libaio
Requires:      /bin/sh
Requires:      /bin/sh
#Requires:      rpmlib(FileDigests) <= 4.6.0-1
#Requires:      rpmlib(PayloadFilesHavePrefix) <= 4.0-1
#Requires:      rpmlib(CompressedFileNames) <= 3.0.4-1
#Requires:      rpmlib(PayloadIsXz) <= 5.2-1
#suggest
#enhance
```

After:
```
Conflicts:     oracle-xe
Conflicts:     oracle-database-xe-18c
Conflicts:     oracle-database-ee-18c
Conflicts:     oracle-database-ee-19c
Conflicts:     oracle-database-ee-20c
Provides:      libclntsh.so.19.1
Provides:      libclntsh.so = 19.1
Provides:      oracle-instantclient19.3-basic = 19.3.0.0.0-1
Provides:      oracle-instantclient19.3-basic(x86-64) = 19.3.0.0.0-1
Requires:      libaio
Requires:      /bin/sh
Requires:      /bin/sh
#Requires:      rpmlib(FileDigests) <= 4.6.0-1
#Requires:      rpmlib(PayloadFilesHavePrefix) <= 4.0-1
#Requires:      rpmlib(CompressedFileNames) <= 3.0.4-1
#Requires:      rpmlib(PayloadIsXz) <= 5.2-1
#suggest
#enhance
```

After modifications:
```
$ rpm -qp --conflicts oracle-instantclient19.3-basic-19.3.0.0.0-1.x86_64.rpm
oracle-xe
oracle-database-xe-18c
oracle-database-ee-18c
oracle-database-ee-19c
oracle-database-ee-20c
```
